val load_xml: ?ns:bool -> string -> Value.t
val load_xml_subst: ?ns:bool -> string -> 
  (Ns.Uri.t * Ns.Uri.t) list -> Value.t
val load_html: string -> Value.t


(* To define and register a parser *)

val xml_parser: (string -> unit) ref

val start_element_handler : string -> (string * string) list -> unit
val end_element_handler : 'a -> unit
val text_handler : string -> unit

