type Biblio  = <bibliography>[Heading Paper*];;
type Heading = <heading>[ PCDATA ];;
type Paper   = <paper>[Author+ Title ((Conference Series?) |(Journal Volume? Number)) Publisher? Year File Abstract?];;
type Author  = <author>[ PCDATA ];;
type Title   = <title>[ PCDATA ];;
type Conference = <conference>[ PCDATA ];;
type Series = <series>[ PCDATA ];;
type Journal = <journal>[ PCDATA ];;
type Publisher = <publisher>[ PCDATA ];;
type Volume  = <volume>[ Int ];;
type Number  = <number>[ Int ];;
type Year  = <year>[ 1970--2010 ];;
type File    = <file>[ PCDATA ];;
type Abstract= <abstract> Text;;
type Text    = [ PCDATA ];; 


type Html  = <html>[Head? Body];;
type Head  = <head>[ <title>[ PCDATA ] ];;
type Body  = <body>[Mix*];;
type Mix   = <h1>[Mix*]
           | <a href=String>[Mix*]
           | <p>[Mix*]
           | <em>[Mix*]
           | <ul>[ <li>[Mix*] +]
           | Char;;

let fun do_authors ([Author+] -> [Mix*])
 | [ <author>a ] -> a
 | [ <author>a <author>b ] -> a @ " and, " @ b
 | [ <author>a; x] -> a @ ", " @ (do_authors x);;

let fun do_paper (Paper -> <li>[Mix*])
 | <paper>[ x::_* <title>t <_>c _* <year>_ <file>f _* ] ->
(* Here, type inference says:  x : [Author+] ... *)
    let authors = do_authors x in
    <li>([ <a href=f>t ] @ authors @ "; in " @ [ <em>c ] @ "." );;


let fun do_biblio (Biblio -> Html)
  <bibliography>[ <heading>h; p ] ->
     let body = match p with
      | [] -> "Empty bibliography"
      | l -> [ <h1>h <ul>(map l with x -> do_paper x) ]
      in
      <html>[ <head>[ <title>h ] <body>body ];;

let bib : Biblio = 
  <bibliography>[
    <heading>"Alain Frisch's bibliography"
    <paper>[
      <author>"Alain Frisch"
(*      <author>"Giuseppe Castagna"
      <author>"V�ronique Benzaken" *)
      <title>"Semantic subtyping"
      <conference>"LICS 02"
      <year>[2002]
      <file>"semsub.ps.gz"
      <abstract>[ 'In this work,...' ]
    ]
(*
    <paper>[
      <author>"Mariangiola Dezani-Ciancaglini"
      <author>"Alain Frisch"
      <author>"Elio Giovannetti"
      <author>"Yoko Motohama"
      <title>"The Relevance of Semantic Subtyping"
      <conference>"ITRS'02"
      <year>[2002]
      <file>"itrs02.ps.gz"
    ]
    <paper>[
      <author>"V�ronique Benzaken"
      <author>"Giuseppe Castagna"
      <author>"Alain Frisch"
      <title>"CDuce: a white-paper"
      <conference>"PLANX-02"
      <year>[2002]
      <file>"planx.ps.gz"
    ]
*)
 ];;

do_biblio bib
;;

(*

[bib]/<papr>_/<author>_;;

*)