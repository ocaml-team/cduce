(*
List the number of items auctioned each month in 1999 for which data is available, ordered by month.

*)



(* functions *)


let fun count(x : [Any*]) : Int =
  let tr_count((Int,[Any*]) -> Int)
        |  (n,[]) -> n
        |  (n,[_;t]) -> tr_count(n+1,t)
   in tr_count(0,x);;



let fun member ((Any,Any) -> Bool)
        |(s,[h;t]) -> if (s=h) then `true else member (s,t)
        |(_,_) -> `false;;

let fun distinct_values ([Any*] -> [Any*])
        | l -> let fun aux (([Any*],[Any*])->[Any*])
                 | ([h;t],l2) ->if member(h,t) then aux(t,l2)
                                            else aux(t,l2@[h])
                 | ([],l2) -> l2
             in aux(l,[]);;

(* tri a bulle *) 

let fun passe ([Any*] -> [Any*])
	  []->[]
	| [a] -> [a]
	| [a b;c] -> if a << b then [a] @ passe ( [b] @ c)
			       else [b] @ passe ( [a] @ c) ;; 
	
let fun order ([Any*] -> [Any*])
	 l -> let l2=passe(l) in
	      if l = l2 then l
	                else order(l2);;


(**********************)

type Year = ['1'--'9' '0'--'9' '0'--'9' '0'--'9'];;

type Month = [('0' '1'--'9'| '1' '0'--'2')] ;;

type Day = [('0' '1'--'9'| '1'--'2' '0'--'9'|'30'|'31')];;

type Date = ['1'--'9' '0'--'9' '0'--'9' '0'--'9' '-' ('0' '1'--'9'| '1' '0'--'2') '-' ('0' '1'--'9'| '1'--'2' '0'--'9'|'30'|'31')];;


let fun get-day-from-date(Date -> Day)
   |[_*  '-' d::(_ _)] ->d
;;
let fun get-month-from-date(Date -> Month)
   |[_* '-' m::(_ _) '-'_*] ->m
;;
let fun get-year-from-date(Date -> Year)
   |[y::(_ _ _ _) _*] -> y
;;

(* In CQLx based on the XQUERY's query)

let end_dates = [items]/Item_tuple/End_date
in <result>
   select <monthly_result>[<month>m <item_count>counting] 
   from m in order(distinct_values(
             select get-month-from-date([e]/Char & Date)
             from e in end_dates )),
        counting in let x=count(
                    select x
                    from x in end_dates
                    where get-year-from-date([x]/Char & Date) ="1999"
                      and get-month-from-date([x]/Char & Date)= m
                    )in [x]
;;

(* In CQLp *)
let months = select months
             from <items>[i::Item_tuple*] in [items],
                  <item_tuple>[ _* e::End_date ;_ ] in i,   
                  <end_date>['1999-' months::(_ _) '-'_*] in e 
in <result>
       select <monthly_result>[
                   <month>m 
                   <item_count>(count(select m2
                                      from m2 in months 
                                      where m=m2))] 
       from m   in order(distinct_values(months))
;;



(* in XQUERY 

<result>
  {
    let $end_dates := doc("items.xml")//item_tuple/end_date
    for $m in distinct-values(for $e in $end_dates 
                              return get-month-from-date($e))
    let $item := doc("items.xml")
        //item_tuple[get-year-from-date(end_date) = 1999 
                     and get-month-from-date(end_date) = $m]
    order by $m
    return
        <monthly_result>
            <month>{ $m }</month>
            <item_count>{ count($item) }</item_count>
        </monthly_result>
  }
</result>

*) 
