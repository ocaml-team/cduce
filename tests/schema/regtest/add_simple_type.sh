test -z "$1" && exit 1
cat SIMPLE_TYPE.xsd.tpl | sed s/ZACK/$1/g > $1.xsd
cat SIMPLE_TYPE.xml.tpl | sed s/ZACK/$1/g > $1.xml
