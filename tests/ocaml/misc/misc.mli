type cduce_int = Big_int.big_int

val f : (char -> char) -> (string -> string)
val x : char ref

val g : char ref -> unit

type complex = { x : float; y : float }
val re : complex -> float
val diag : float -> complex

val map_complex : (float * float -> float) -> complex -> float

type t = A of t | B of t * t | C of int
val pp : t -> string

val find : t -> int -> bool

val i : int

val exists : string -> bool

val home : string

val stdin : Unix.file_descr

val unix_write : Unix.file_descr -> string -> int -> int -> int

val str_len : string -> int

val listmap : (cduce_int -> cduce_int) -> cduce_int list -> cduce_int list

type typ =
  | Empty of unit
  | Tree of string * typ * typ

val visit_tree_infix : (string -> string) -> typ -> string

type point3d = { a : float; b : float; c : float }

type stack3d =
  | Novalue of unit
  | Stack of point3d * stack3d

val push : stack3d -> point3d -> stack3d
val pop : stack3d -> stack3d
val print_stack : stack3d -> string
