open OUnit2

let tests = "Cdsdl" >:::
  [
    "sdl_version" >:: ( fun test_ctxt ->
      try
	assert_equal ~msg:"Test Cdsdl.sdl_version.1 failed"
	  (Sdl.string_of_version (Sdl.version ())) (Cdsdl.sdl_version);
      with (ex) -> print_string "SDL exception\n"
    );

    "sdl_num_drives" >:: ( fun test_ctxt ->
      try
	assert_equal ~msg:"Test Cdsdl.sdl_num_drives.1 failed"
	  (Sdlcdrom.get_num_drives ()) (Cdsdl.sdl_num_drives);
      with (ex) -> print_string "SDL exception\n"
    );

    "sdl_complete_example" >:: ( fun test_ctxt ->
      try
	Cdsdl.sdl_init;
	let cd = Cdsdl.sdl_cd_open 0 in
	Cdsdl.sdl_cd_eject cd;
	Cdsdl.sdl_quit ();
      with (ex) -> print_string "SDL exception\n"
    );
  ]

let _ = run_test_tt_main tests
