open OUnit2

let tests = "Eval" >:::
  [
    "misc" >:: ( fun test_ctxt ->
      let el = Eval.B("test") in
      let el2 = Eval.A(Eval.A(Eval.B("test"),Eval.B("test2")),Eval.B("test3"))
      in
      let rec to_str x = match x with
	| Eval.B(str) -> str
	| Eval.A(e1, e2) -> "(" ^ (to_str e1) ^ ", " ^ (to_str e2) ^ ")"
      in
      assert_equal ~msg:"Test Eval.misc.1 failed" "test" (to_str el);
      assert_equal ~msg:"Test Eval.misc.2 failed"
	"((test, test2), test3)" (to_str el2);
    );
  ]

let _ = run_test_tt_main tests
