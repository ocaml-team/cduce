(* An implementation of the OCaml's Location signature (to cut dependencies
   to other OCaml modules *)

open Lexing
type t = { loc_start: position; loc_end: position; loc_ghost: bool }
type 'a loc = { txt: 'a; loc: t }

let none = { loc_start = dummy_pos; loc_end = dummy_pos; loc_ghost = true }
let dummy x = assert false
let in_file = dummy
let init = dummy
let curr = dummy
let symbol_rloc = dummy
let symbol_gloc = dummy
let rhs_loc = dummy
let input_name = ref ""
let input_lexbuf = ref None
let get_pos_info = dummy
let print_error_cur_file = dummy
let print_error = dummy
let print = dummy
let print_warning = dummy
let prerr_warning = dummy
let echo_eof = dummy
let reset = dummy
let highlight_locations = dummy
let mknoloc = dummy
let mkloc = dummy
let print_loc = dummy
let print_filename = dummy
let show_filename = dummy
let absname = ref true
