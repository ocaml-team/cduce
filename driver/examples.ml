
let examples = [ "q1","
type Bib = <bib>[Book*]
type Book = <book year=String>[Title  (Author+ | Editor+ ) Publisher Price ]
type Author = <author>[Last  First ]
type Editor = <editor>[Last  First  Affiliation ]
type Title  = <title>[PCDATA ]
type Last  = <last>[PCDATA]
type First  = <first>[PCDATA]
type Affiliation = <affiliation>[PCDATA]
type Publisher  = <publisher>[PCDATA]
type Price  = <price>[PCDATA];;

let doc : Bib = <bib>[
             <book year=\"1994\">[
                  <title>['TCP/IP Illustrated']
                  <author>[<last>['Stevens']
                  <first>['W.']]
                  <publisher>['Addison-Wesley']
                  <price>['65.95']
             ]
             <book year=\"1992\">[
                  <title>['Advanced Programming in the Unix environment']
                  <author>[<last>['Stevens'] <first>['W.']]
                  <publisher>['Addison-Wesley']
                  <price>['65.95']
             ]
             <book year=\"2000\">[ 
                  <title>['Data on the Web']
                  <author>[<last>['Abiteboul'] <first>['Serge']]
                  <author>[<last>['Buneman'] <first>['Peter']]
                  <author>[<last>['Suciu'] <first>['Dan']]
                  <publisher>['Morgan Kaufmann Publishers']
                  <price>['39.95']
            ]
            <book year=\"1999\">[
                  <title>['The Economics of Technology and Content for Digital TV']
                  <editor>[
                      <last>['Gerbarg'] <first>['Darcy']
                      <affiliation>['CITI']
                  ]
                  <publisher>['Kluwer Academic Publishers']
                  <price>['129.95']
             ]];;
	     
	     
	     
(*
-----------------------------Q1-----------------------------
List books published by Addison-Wesley after 1991, including their year and title.
------------------------------------------------------------

<bib>
 {
  for $b in doc(\"http://bstore1.example.com/bib.xml\")/bib/book
  where $b/publisher = \"Addison-Wesley\" and $b/@year > 1991
  return
    <book year=\"{ $b/@year }\">
     { $b/title }
    </book>
 }
</bib> 

*)
<bib>
select <book year=y>([b]/Title)
from b in [doc]/<book .. >[_* <publisher>\"Addison-Wesley\";_] , 
     y in [b]/@year 
where 1991<<int_of(y);;

<bib> 
select <book year=y> t 
from <bib>[b::Book*] in [doc], 
     <book year=y .. >[t::Title _+ <publisher>\"Addison-Wesley\";_] in b 
where 1991 << int_of(y);;

";"q2","
type Bib = <bib>[Book*]
type Book = <book year=String>[Title  (Author+ | Editor+ ) Publisher Price ]
type Author = <author>[Last  First ]
type Editor = <editor>[Last  First  Affiliation ]
type Title  = <title>[PCDATA ]
type Last  = <last>[PCDATA]
type First  = <first>[PCDATA]
type Affiliation = <affiliation>[PCDATA]
type Publisher  = <publisher>[PCDATA]
type Price  = <price>[PCDATA];;

let doc : Bib = <bib>[
             <book year=\"1994\">[
                  <title>['TCP/IP Illustrated']
                  <author>[<last>['Stevens']
                  <first>['W.']]
                  <publisher>['Addison-Wesley']
                  <price>['65.95']
             ]
             <book year=\"1992\">[
                  <title>['Advanced Programming in the Unix environment']
                  <author>[<last>['Stevens'] <first>['W.']]
                  <publisher>['Addison-Wesley']
                  <price>['65.95']
             ]
             <book year=\"2000\">[ 
                  <title>['Data on the Web']
                  <author>[<last>['Abiteboul'] <first>['Serge']]
                  <author>[<last>['Buneman'] <first>['Peter']]
                  <author>[<last>['Suciu'] <first>['Dan']]
                  <publisher>['Morgan Kaufmann Publishers']
                  <price>['39.95']
            ]
            <book year=\"1999\">[
                  <title>['The Economics of Technology and Content for Digital TV']
                  <editor>[
                      <last>['Gerbarg'] <first>['Darcy']
                      <affiliation>['CITI']
                  ]
                  <publisher>['Kluwer Academic Publishers']
                  <price>['129.95']
             ]];;
	     
	     
	     
(*
---------------------------Q2---------------------------------
Create a flat list of all the title-author pairs, with each pair enclosed in a \"result\" element.
--------------------------------------------------------------

<results>
  {
    for $b in doc(\"http://bstore1.example.com/bib.xml\")/bib/book,
        $t in $b/title,
        $a in $b/author
    return
        <result>
            { $t }    
            { $a }
        </result>
  }
</results>

*)

<results>
select <result>([b]/Title @ [a]) 
from b in [doc]/Book,
     a in [b]/Author ;;


<results>
 select <result>[t a] 
 from <bib>[b::Book+] in [doc], 
      <book ..>[t&Title la::Author+ ;_] in b,
      a in la ;;

";"q3","
type Bib = <bib>[Book*]
type Book = <book year=String>[Title  (Author+ | Editor+ ) Publisher Price ]
type Author = <author>[Last  First ]
type Editor = <editor>[Last  First  Affiliation ]
type Title  = <title>[PCDATA ]
type Last  = <last>[PCDATA]
type First  = <first>[PCDATA]
type Affiliation = <affiliation>[PCDATA]
type Publisher  = <publisher>[PCDATA]
type Price  = <price>[PCDATA];;

let doc : Bib = <bib>[
             <book year=\"1994\">[
                  <title>['TCP/IP Illustrated']
                  <author>[<last>['Stevens']
                  <first>['W.']]
                  <publisher>['Addison-Wesley']
                  <price>['65.95']
             ]
             <book year=\"1992\">[
                  <title>['Advanced Programming in the Unix environment']
                  <author>[<last>['Stevens'] <first>['W.']]
                  <publisher>['Addison-Wesley']
                  <price>['65.95']
             ]
             <book year=\"2000\">[ 
                  <title>['Data on the Web']
                  <author>[<last>['Abiteboul'] <first>['Serge']]
                  <author>[<last>['Buneman'] <first>['Peter']]
                  <author>[<last>['Suciu'] <first>['Dan']]
                  <publisher>['Morgan Kaufmann Publishers']
                  <price>['39.95']
            ]
            <book year=\"1999\">[
                  <title>['The Economics of Technology and Content for Digital TV']
                  <editor>[
                      <last>['Gerbarg'] <first>['Darcy']
                      <affiliation>['CITI']
                  ]
                  <publisher>['Kluwer Academic Publishers']
                  <price>['129.95']
             ]];;
	     
	     
(*
---------------------------Q3---------------------------------
For each book in the bibliography, list the title and authors, grouped inside a \"result\" element.
--------------------------------------------------------------
<results>
{
    for $b in doc(\"http://bstore1.example.com/bib.xml\")/bib/book
    return
        <result>
            { $b/title }
            { $b/author  }
        </result>
}
</results> 


*)

<results>
select <result> ([b]/Title @ [b]/Author )
from b in [doc]/Book;;



<results>
select <result>(t @ a)
from <bib>[b::Book*] in [doc],
     <book ..>[t::Title (a::Author+|_)* ;_] in b
;;




";"q4","
type Bib = <bib>[Book*]
type Book = <book year=String>[Title  (Author+ | Editor+ ) Publisher Price ]
type Author = <author>[Last  First ]
type Editor = <editor>[Last  First  Affiliation ]
type Title  = <title>[PCDATA ]
type Last  = <last>[PCDATA]
type First  = <first>[PCDATA]
type Affiliation = <affiliation>[PCDATA]
type Publisher  = <publisher>[PCDATA]
type Price  = <price>[PCDATA];;

let doc : Bib = <bib>[
             <book year=\"1994\">[
                  <title>['TCP/IP Illustrated']
                  <author>[<last>['Stevens']
                  <first>['W.']]
                  <publisher>['Addison-Wesley']
                  <price>['65.95']
             ]
             <book year=\"1992\">[
                  <title>['Advanced Programming in the Unix environment']
                  <author>[<last>['Stevens'] <first>['W.']]
                  <publisher>['Addison-Wesley']
                  <price>['65.95']
             ]
             <book year=\"2000\">[ 
                  <title>['Data on the Web']
                  <author>[<last>['Abiteboul'] <first>['Serge']]
                  <author>[<last>['Buneman'] <first>['Peter']]
                  <author>[<last>['Suciu'] <first>['Dan']]
                  <publisher>['Morgan Kaufmann Publishers']
                  <price>['39.95']
            ]
            <book year=\"1999\">[
                  <title>['The Economics of Technology and Content for Digital TV']
                  <editor>[
                      <last>['Gerbarg'] <first>['Darcy']
                      <affiliation>['CITI']
                  ]
                  <publisher>['Kluwer Academic Publishers']
                  <price>['129.95']
             ]];;
	     
	     
(*
--------------------------------Q4------------------------------
For each author in the bibliography, list the author`s name and the titles 
of all books by that author, grouped inside a \"result\" element.
----------------------------------------------------------------
<results>
  {
    let $a := doc(\"http://bstore1.example.com/bib/bib.xml\")//author
    for $last in distinct-values($a/last),
        $first in distinct-values($a[last=$last]/first)
    order by $last, $first
    return
        <result>
            <author>
               <last>{ $last }</last>
               <first>{ $first }</first>
            </author>
            {
                for $b in doc(\"http://bstore1.example.com/bib.xml\")/bib/book
                where some $ba in $b/author 
                      satisfies ($ba/last = $last and $ba/first=$first)
                return $b/title
            }
        </result>
  }
</results> 
*)

<results>
select <result> ([a] @
                  flatten(select [b]/Title
                  from b in [doc]/Book
                  where member(a,[b]/Author) ))
from a in distinct_values( [doc]/Book/Author );;


<results>
select <result>([a] @(select t
                     from <bib>[l::Book+] in [doc],
                          <book ..>[t&Title a2::Author+ ;_] in l
                     where member(a,a2) ))                                        
from a in distinct_values(
          select a
          from <bib>[b::Book+] in [doc],
               <book ..>[Title la::Author+ ;_ ] in b,
               a in la);;




";"q5","
type Bib = <bib>[Book*]
type Book = <book year=String>[Title  (Author+ | Editor+ ) Publisher Price ]
type Author = <author>[Last  First ]
type Editor = <editor>[Last  First  Affiliation ]
type Title  = <title>[PCDATA ]
type Last  = <last>[PCDATA]
type First  = <first>[PCDATA]
type Affiliation = <affiliation>[PCDATA]
type Publisher  = <publisher>[PCDATA]
type Price  = <price>[PCDATA];;

let doc : Bib = <bib>[
             <book year=\"1994\">[
                  <title>['TCP/IP Illustrated']
                  <author>[<last>['Stevens']
                  <first>['W.']]
                  <publisher>['Addison-Wesley']
                  <price>['65.95']
             ]
             <book year=\"1992\">[
                  <title>['Advanced Programming in the Unix environment']
                  <author>[<last>['Stevens'] <first>['W.']]
                  <publisher>['Addison-Wesley']
                  <price>['65.95']
             ]
             <book year=\"2000\">[ 
                  <title>['Data on the Web']
                  <author>[<last>['Abiteboul'] <first>['Serge']]
                  <author>[<last>['Buneman'] <first>['Peter']]
                  <author>[<last>['Suciu'] <first>['Dan']]
                  <publisher>['Morgan Kaufmann Publishers']
                  <price>['39.95']
            ]
            <book year=\"1999\">[
                  <title>['The Economics of Technology and Content for Digital TV']
                  <editor>[
                      <last>['Gerbarg'] <first>['Darcy']
                      <affiliation>['CITI']
                  ]
                  <publisher>['Kluwer Academic Publishers']
                  <price>['129.95']
             ]];;
	     
	     
(*
---------------------------Q5------------------------------------
For each book found at both bstore1.example.com and bstore2.example.com, 
list the title of the book and its price from each source.
-----------------------------------------------------------------

<books-with-prices>
  {
    for $b in doc(\"http://bstore1.example.com/bib.xml\")//book,
        $a in doc(\"http://bstore2.example.com/reviews.xml\")//entry
    where $b/title = $a/title
    return
        <book-with-prices>
            { $b/title }
            <price-bstore2>{ $a/price/text() }</price-bstore2>
            <price-bstore1>{ $b/price/text() }</price-bstore1>
        </book-with-prices>
  }
</books-with-prices>

*)



type Reviews =<reviews>[Entry*];;
type Entry = <entry> [ Title Price Review];;
(*type Title = <title>[PCDATA];;*)
(*type Price= <price>[PCDATA];;*)
type Review =<review>[PCDATA];;

let amazon=
<reviews>[
    <entry>[
        <title>['Data on the Web']
        <price>['34.95']
        <review>
               ['A very good discussion of semi-structured database
               systems and XML.']
     ]
    <entry>[
        <title>['Advanced Programming in the Unix environment']
        <price>['65.95']
        <review>
               ['A clear and detailed discussion of UNIX programming.']
     ]
    <entry>[
        <title>['TCP/IP Illustrated']
        <price>['65.95']
        <review>
               ['One of the best books on TCP/IP.']
    ]
];;

<books-with-prices>
select <book-with-price>[t1 <price-amazon>([p2]/Char) <price-bn>([p1]/Char)]
from b in [doc]/Book ,
     y in [b]/@year,
     t1 in [b]/Title,
     e in [amazon]/Entry,
     t2 in [e]/Title,
     p2 in [e]/Price,
     p1 in [b]/Price
 where t1=t2;;



<books-with-prices>
select <book-with-price>[t2 <price-amazon>p2 <price-bn>p1 ]
from <bib>[b::Book*] in [doc],
     <book year=y ..>[t1&Title _* <price>p1] in b,
     <reviews>[e::Entry*] in [amazon],
     <entry>[t2&Title <price>p2 ;_] in e
where t1=t2;;



";"q6","
type Bib = <bib>[Book*]
type Book = <book year=String>[Title  (Author+ | Editor+ ) Publisher Price ]
type Author = <author>[Last  First ]
type Editor = <editor>[Last  First  Affiliation ]
type Title  = <title>[PCDATA ]
type Last  = <last>[PCDATA]
type First  = <first>[PCDATA]
type Affiliation = <affiliation>[PCDATA]
type Publisher  = <publisher>[PCDATA]
type Price  = <price>[PCDATA];;

let doc : Bib = <bib>[
             <book year=\"1994\">[
                  <title>['TCP/IP Illustrated']
                  <author>[<last>['Stevens']
                  <first>['W.']]
                  <publisher>['Addison-Wesley']
                  <price>['65.95']
             ]
             <book year=\"1992\">[
                  <title>['Advanced Programming in the Unix environment']
                  <author>[<last>['Stevens'] <first>['W.']]
                  <publisher>['Addison-Wesley']
                  <price>['65.95']
             ]
             <book year=\"2000\">[ 
                  <title>['Data on the Web']
                  <author>[<last>['Abiteboul'] <first>['Serge']]
                  <author>[<last>['Buneman'] <first>['Peter']]
                  <author>[<last>['Suciu'] <first>['Dan']]
                  <publisher>['Morgan Kaufmann Publishers']
                  <price>['39.95']
            ]
            <book year=\"1999\">[
                  <title>['The Economics of Technology and Content for Digital TV']
                  <editor>[
                      <last>['Gerbarg'] <first>['Darcy']
                      <affiliation>['CITI']
                  ]
                  <publisher>['Kluwer Academic Publishers']
                  <price>['129.95']
             ]];;
	     
(*	     
-------------------------------Q6----------------------------------
For each book that has at least one author, list the title and first two authors, 
and an empty \"et-al\" element if the book has additional authors.
-------------------------------------------------------------------
<bib>
  {
    for $b in doc(\"http://bstore1.example.com/bib.xml\")//book
    where count($b/author) > 0
    return
        <book>
            { $b/title }
            {
                for $a in $b/author[position()<=2]  
                return $a
            }
            {
                if (count($b/author) > 2)
                 then <et-al/>
                 else ()
            }
        </book>
  }
</bib>

*)


<bib>
select <book>[t  !a  !etal]
from <bib>[b::Book*] in [doc],
     <book ..>[t&Title a::(Author Author) Author&(etal:=[<et-al>[]]) ;_] | <book ..>[t&Title (a::(Author+)) (etal:=[])  _*] in b;;

";"q8","
type Bib = <bib>[Book*]
type Book = <book year=String>[Title  (Author+ | Editor+ ) Publisher Price ]
type Author = <author>[Last  First ]
type Editor = <editor>[Last  First  Affiliation ]
type Title  = <title>[PCDATA ]
type Last  = <last>[PCDATA]
type First  = <first>[PCDATA]
type Affiliation = <affiliation>[PCDATA]
type Publisher  = <publisher>[PCDATA]
type Price  = <price>[PCDATA];;

let doc : Bib = <bib>[
             <book year=\"1994\">[
                  <title>['TCP/IP Illustrated']
                  <author>[<last>['Stevens']
                  <first>['W.']]
                  <publisher>['Addison-Wesley']
                  <price>['65.95']
             ]
             <book year=\"1992\">[
                  <title>['Advanced Programming in the Unix environment']
                  <author>[<last>['Stevens'] <first>['W.']]
                  <publisher>['Addison-Wesley']
                  <price>['65.95']
             ]
             <book year=\"2000\">[ 
                  <title>['Data on the Web']
                  <author>[<last>['Abiteboul'] <first>['Serge']]
                  <author>[<last>['Buneman'] <first>['Peter']]
                  <author>[<last>['Suciu'] <first>['Dan']]
                  <publisher>['Morgan Kaufmann Publishers']
                  <price>['39.95']
            ]
            <book year=\"1999\">[
                  <title>['The Economics of Technology and Content for Digital TV']
                  <editor>[
                      <last>['Gerbarg'] <first>['Darcy']
                      <affiliation>['CITI']
                  ]
                  <publisher>['Kluwer Academic Publishers']
                  <price>['129.95']
             ]];;
	     
(*
---------------------------------Q8-----------------------------------
Find books in which the name of some element ends with the string \"or\" 
and the same element contains the string \"Suciu\" somewhere in its content. 
For each such book, return the title and the qualifying element.
-----------------------------------------------------------------------


for $b in doc(\"http://bstore1.example.com/bib.xml\")//book
let $e := $b/*[contains(string(.), \"Suciu\") 
               and ends-with(local-name(.), \"or\")]
where exists($e)
return
    <book>
        { $b/title }
        { $e }
    </book> 

*)



<result> 
select <book>[t element ]
from <bib>[b::Book*]in [doc],
     <book ..>[t&Title elements::Any*] in b,
     <(atom)>_&element  in elements,
     [_* 'or']          in [ (string_of(atom)) ],
     <_>[_* 'Suciu' _*] in [element]//<_>[Char*];;


";"q9","(*-----------------------------Q9-------------------------------------
In the document \"books.xml\", find all section or chapter titles that 
contain the word \"XML\", regardless of the level of nesting.
--------------------------------------------------------------------
<results>
  {
    for $t in doc(\"books.xml\")//(chapter | section)/title
    where contains($t/text(), \"XML\")
    return $t
  }
</results> 
*)




type Chapter = <chapter>[Title Section*]
type Section = <section>[Title Section*]
type Title = <title>[PCDATA];;
 


let doc : Chapter= 
<chapter>[
    <title>['Data Model']
      <section>[
        <title>['Syntax For Data Model']
      ]
      <section>[
        <title>['XML']
        <section>[
            <title>['Basic Syntax']
        ]
        <section>[
            <title>['XML and Semistructured Data']
        ]
      ]
];;

select  t
from t in [doc]//Section/<title>[_* 'XML' _* ];;
";"q11","
type Bib = <bib>[Book*]
type Book = <book year=String>[Title  (Author+ | Editor+ ) Publisher Price ]
type Author = <author>[Last  First ]
type Editor = <editor>[Last  First  Affiliation ]
type Title  = <title>[PCDATA ]
type Last  = <last>[PCDATA]
type First  = <first>[PCDATA]
type Affiliation = <affiliation>[PCDATA]
type Publisher  = <publisher>[PCDATA]
type Price  = <price>[PCDATA];;

let doc : Bib = <bib>[
             <book year=\"1994\">[
                  <title>['TCP/IP Illustrated']
                  <author>[<last>['Stevens']
                  <first>['W.']]
                  <publisher>['Addison-Wesley']
                  <price>['65.95']
             ]
             <book year=\"1992\">[
                  <title>['Advanced Programming in the Unix environment']
                  <author>[<last>['Stevens'] <first>['W.']]
                  <publisher>['Addison-Wesley']
                  <price>['65.95']
             ]
             <book year=\"2000\">[ 
                  <title>['Data on the Web']
                  <author>[<last>['Abiteboul'] <first>['Serge']]
                  <author>[<last>['Buneman'] <first>['Peter']]
                  <author>[<last>['Suciu'] <first>['Dan']]
                  <publisher>['Morgan Kaufmann Publishers']
                  <price>['39.95']
            ]
            <book year=\"1999\">[
                  <title>['The Economics of Technology and Content for Digital TV']
                  <editor>[
                      <last>['Gerbarg'] <first>['Darcy']
                      <affiliation>['CITI']
                  ]
                  <publisher>['Kluwer Academic Publishers']
                  <price>['129.95']
             ]];;
	     
	     
	     
(*
--------------------------------Q11------------------------------------------
For each book with an author, return the book with its title and authors. 
For each book with an editor, return a reference with the book title and the editor`s affiliation.
-------------------------------------------------------------------------------
<bib>
{
        for $b in doc(\"http://bstore1.example.com/bib.xml\")//book[author]
        return
            <book>
                { $b/title }
                { $b/author }
            </book>
}
{
        for $b in doc(\"http://bstore1.example.com/bib.xml\")//book[editor]
        return
          <reference>
            { $b/title }
            {$b/editor/affiliation}
          </reference>
}
</bib>  

*)

<bib>((
select <book>(([b]/Title) @ ([b]/Author))
from b in [doc]/<book ..>[_ Author;_])
@
(select <reference>(([b]/Title) @ ([b]/Editor/Affiliation))
from b in [doc]/<book ..>[_ Editor;_]     
));;


<bib>((
select <book>([t] @ la)
from <bib>[b::Book+] in [doc],
     <book ..>[t&Title la::Author+ ;_] in b
)@(
select <reference>[t aff]
from <bib>[b::Book+] in [doc],
     <book ..>[t&Title _* <editor>[_* aff&Affiliation] ;_] in b));;



<bib>
select <(tag)>([t] @ [a1] @a2)
from <book ..>[t&Title ((tag:=`book)&a1&Author a2::Author* | (tag:=`reference)&a1&Editor a2::Editor*) _*] in [doc]/Book;;
		       


";"q12","type Bib = <bib>[Book*]
type Book = <book year=String>[Title  (Author+ | Editor+ ) Publisher Price ]
type Author = <author>[Last  First ]
type Editor = <editor>[Last  First  Affiliation ]
type Title  = <title>[PCDATA ]
type Last  = <last>[PCDATA]
type First  = <first>[PCDATA]
type Affiliation = <affiliation>[PCDATA]
type Publisher  = <publisher>[PCDATA]
type Price  = <price>[PCDATA];;

let doc : Bib = <bib>[
             <book year=\"1994\">[
                  <title>['TCP/IP Illustrated']
                  <author>[<last>['Stevens']
                  <first>['W.']]
                  <publisher>['Addison-Wesley']
                  <price>['65.95']
             ]
             <book year=\"1992\">[
                  <title>['Advanced Programming in the Unix environment']
                  <author>[<last>['Stevens'] <first>['W.']]
                  <publisher>['Addison-Wesley']
                  <price>['65.95']
             ]
             <book year=\"2000\">[ 
                  <title>['Data on the Web']
                  <author>[<last>['Abiteboul'] <first>['Serge']]
                  <author>[<last>['Buneman'] <first>['Peter']]
                  <author>[<last>['Suciu'] <first>['Dan']]
                  <publisher>['Morgan Kaufmann Publishers']
                  <price>['39.95']
            ]
            <book year=\"1999\">[
                  <title>['The Economics of Technology and Content for Digital TV']
                  <editor>[
                      <last>['Gerbarg'] <first>['Darcy']
                      <affiliation>['CITI']
                  ]
                  <publisher>['Kluwer Academic Publishers']
                  <price>['129.95']
             ]];;


(*
---------------------------------------------Q12----------------------------------------
Find pairs of books that have different titles but the same set of authors (possibly in a different order).
------------------------------------------------------------------------------------------

<bib>
{
    for $book1 in doc(\"http://bstore1.example.com/bib.xml\")//book,
        $book2 in doc(\"http://bstore1.example.com/bib.xml\")//book
    let $aut1 := for $a in $book1/author 
                 order by $a/last, $a/first
                 return $a
    let $aut2 := for $a in $book2/author 
                 order by $a/last, $a/first
                 return $a
    where $book1 << $book2
    and not($book1/title = $book2/title)
    and deep-equal($aut1, $aut2) 
    return
        <book-pair>
            { $book1/title }
            { $book2/title }
        </book-pair>
}
</bib> 
*)


flatten(
select   select <book-pair>[!([b1]/Title) !([b2]/Title) ]
          from a in [b2]/Author
          where (member(a,[b1]/Author))

 
from b1 in [doc]/Book,
     b2 in [doc]/Book
where not([b1]/Title = [b2]/Title)
)


(*
reste a retirer les doublons. besoin de l op�rateur order pour ca.
*)






";"qatt1","type Bib = <bib>[Book*]
type Book = <book year=String>[Title  (Author+ | Editor+ ) Publisher Price ]
type Author = <author>[Last  First ]
type Editor = <editor>[Last  First  Affiliation ]
type Title  = <title>[PCDATA ]
type Last  = <last>[PCDATA]
type First  = <first>[PCDATA]
type Affiliation = <affiliation>[PCDATA]
type Publisher  = <publisher>[PCDATA]
type Price  = <price>[PCDATA];;

let doc : Bib = <bib>[
             <book year=\"1994\">[
                  <title>['TCP/IP Illustrated']
                  <author>[<last>['Stevens']
                  <first>['W.']]
                  <publisher>['Addison-Wesley']
                  <price>['65.95']
             ]
             <book year=\"1992\">[
                  <title>['Advanced Programming in the Unix environment']
                  <author>[<last>['Stevens'] <first>['W.']]
                  <publisher>['Addison-Wesley']
                  <price>['65.95']
             ]
             <book year=\"2000\">[ 
                  <title>['Data on the Web']
                  <author>[<last>['Abiteboul'] <first>['Serge']]
                  <author>[<last>['Buneman'] <first>['Peter']]
                  <author>[<last>['Suciu'] <first>['Dan']]
                  <publisher>['Morgan Kaufmann Publishers']
                  <price>['39.95']
            ]
            <book year=\"1999\">[
                  <title>['The Economics of Technology and Content for Digital TV']
                  <editor>[
                      <last>['Gerbarg'] <first>['Darcy']
                      <affiliation>['CITI']
                  ]
                  <publisher>['Kluwer Academic Publishers']
                  <price>['129.95']
             ]];;
	     


let biblio = [doc]/Book;;

<bib>
    select <book (a)> x
      from <book (a)>[ (x::(Any\Editor)|_ )* ] in biblio ;;


(*This expression returns all book in bib but removoing the editor element. If one wants to write more explicitly:
*)
    select <book (a)> x
      from <book (a)>[ (x::(Any\<editor ..>_)|_ )* ] in biblio;;

(*
Or even:
*)
    select <book (a)> x
      from <book (a)>[ (x::(<(_\`editor) ..>_)|_ )* ] in biblio ;;

";"qatt2","type Bib = <bib>[Book*]
type Book = <book year=String>[Title  (Author+ | Editor+ ) Publisher Price ]
type Author = <author>[Last  First ]
type Editor = <editor>[Last  First  Affiliation ]
type Title  = <title>[PCDATA ]
type Last  = <last>[PCDATA]
type First  = <first>[PCDATA]
type Affiliation = <affiliation>[PCDATA]
type Publisher  = <publisher>[PCDATA]
type Price  = <price>[PCDATA];;

let doc : Bib = <bib>[
             <book year=\"1994\">[
                  <title>['TCP/IP Illustrated']
                  <author>[<last>['Stevens']
                  <first>['W.']]
                  <publisher>['Addison-Wesley']
                  <price>['65.95']
             ]
             <book year=\"1992\">[
                  <title>['Advanced Programming in the Unix environment']
                  <author>[<last>['Stevens'] <first>['W.']]
                  <publisher>['Addison-Wesley']
                  <price>['65.95']
             ]
             <book year=\"2000\">[ 
                  <title>['Data on the Web']
                  <author>[<last>['Abiteboul'] <first>['Serge']]
                  <author>[<last>['Buneman'] <first>['Peter']]
                  <author>[<last>['Suciu'] <first>['Dan']]
                  <publisher>['Morgan Kaufmann Publishers']
                  <price>['39.95']
            ]
            <book year=\"1999\">[
                  <title>['The Economics of Technology and Content for Digital TV']
                  <editor>[
                      <last>['Gerbarg'] <first>['Darcy']
                      <affiliation>['CITI']
                  ]
                  <publisher>['Kluwer Academic Publishers']
                  <price>['129.95']
             ]];;
	     

(*
Back to the first one:
*)
<bib>
    select <book (a)> x
      from <(book) (a)>[ (x::(Any\Editor)|_ )* ] in biblio ;;

(*
This query takes any element in bib, tranforms it in a book element and removes sub-elements editor, but you will get a warning as capture variable book in the from is never used: we should have written <(_) (a)> instead of <(book) (a)> the from
*)
    select <(book) (a)> x
      from <(book) (a)>[ (x::(Any\Editor)|_ )* ] in biblio ;;

";"qatt3","type Bib = <bib>[Book*]
type Book = <book year=String>[Title  (Author+ | Editor+ ) Publisher Price ]
type Author = <author>[Last  First ]
type Editor = <editor>[Last  First  Affiliation ]
type Title  = <title>[PCDATA ]
type Last  = <last>[PCDATA]
type First  = <first>[PCDATA]
type Affiliation = <affiliation>[PCDATA]
type Publisher  = <publisher>[PCDATA]
type Price  = <price>[PCDATA];;

let doc : Bib = <bib>[
             <book year=\"1994\">[
                  <title>['TCP/IP Illustrated']
                  <author>[<last>['Stevens']
                  <first>['W.']]
                  <publisher>['Addison-Wesley']
                  <price>['65.95']
             ]
             <book year=\"1992\">[
                  <title>['Advanced Programming in the Unix environment']
                  <author>[<last>['Stevens'] <first>['W.']]
                  <publisher>['Addison-Wesley']
                  <price>['65.95']
             ]
             <book year=\"2000\">[ 
                  <title>['Data on the Web']
                  <author>[<last>['Abiteboul'] <first>['Serge']]
                  <author>[<last>['Buneman'] <first>['Peter']]
                  <author>[<last>['Suciu'] <first>['Dan']]
                  <publisher>['Morgan Kaufmann Publishers']
                  <price>['39.95']
            ]
            <book year=\"1999\">[
                  <title>['The Economics of Technology and Content for Digital TV']
                  <editor>[
                      <last>['Gerbarg'] <first>['Darcy']
                      <affiliation>['CITI']
                  ]
                  <publisher>['Kluwer Academic Publishers']
                  <price>['129.95']
             ]];;
	     
(*
Same thing but without tranforming tag to \"book\".
More interestingly:
*)


    select <(b) (a\id)> x
      from <(b) (a)>[ (x::(Any\Editor)|_ )* ] in biblio ;;



(*
removes all \"id\" attribute (if any) from the attributes of the element in bib.
*)


    select <(b) (a\id+{bing=a.id})> x
      from <(b) (a)>[ (x::(Any\Editor)|_ )* ] in biblio ;;



(*
Changes attribute id=x into bing=x However, one must be sure that each element in bib has an id attribute if such is not the case the expression is ill-typed. If one wants to perform this only for those elements which certainly have an id attribute then:
*)


    select <(b) (a\id+{bing=a.id})> x
      from <(b) (a&{id=_})>[ (x::(Any\Editor)|_ )* ] in biblio ;;

";"xml","(* Syntax for XML elements *)

type A = <a x=String y=?String>[ B* ]
type B = <b>[ PCDATA A? PCDATA ]

let x : A = 
 <a x=\"Bla\" y=\"Blo\">[ 
   <b>[ 'blabla' ]
   <b>[ 
     <a x=\"Foo\">[] 'bla' 'bla' 
   ] 
 ]
";"functions","(* Simple functions can be defined this way: *)
let f1 (x : Int) : Int = x + 3
;;
f1 5

(* With several arguments: *)
let f2 (x : Int, y : Int) : Int = x + y
;;
f2 (10,20)

(* Currified form *)
let add (x : Int) (y : Int) : Int = x + y
;;
add 10 20

(* You may directly deconstruct the arguments: *)
type A = <a href=String>String
let f3 (<a href=url>txt : A) : String = url @ \"=>\" @ txt
;;
f3 <a href=\"http://www.cduce.org\">\"CDuce homepage\";;

(* In general, if you want to specify several arrow types, or
   use several pattern matching branches, you have the general
   form: *)

let f4 (A -> String; ['0'--'9'+] -> Int)
| x & A -> f3 x
| x -> int_of x
;;
f4 \"123\"
";"mutrec","(* Adjacent type declarations are mutually recursive *)
type T = <t>S
type S = [ (Char | T)* ]
let x : S = [ 'abc' <t>['def'] 'ghi' ]

(* Similarly for toplevel function definitions *)

let f (x : Int) : Int = g x
let g (x : Int) : Int = 3
let a = 2
let h (x : Int) : Int = f x
   (* f and g are mutually recursive, but they cannot use h *)
";"sequence","(* Sequence are just defined with pairs and the atom `nil;
   the following notation are equivalent: *)
let l1 = (1,2,3,`nil)
let l2 = (1,(2,(3,`nil)))
let l3 = [ 1 2 3 ]

(* The [...] notation allow to specify a tail after a semi-colon : *)
let l4 = (10,20,l1)
let l5 = [ 10 20 ; l1 ]

(* Concatenation @ *)
let l6 = [ 1 2 3 ] @ [ 4 5 6 ]

(* Inside [...], it is possible to escape a subsequence with a ! *)
let l7 = [ 1 2 !l6 !l1 5 ]
";"seqtypes","(* Sequence types are defined with regular expression over types *)
type IntList = [ Int* ]
type IntStringList = [ (Int String)* ]
type IntNonEmptyList = [ Int+ ]

let l : IntList = [ 1 2 3 ]
";"integers","(* Yes, CDuce can handle large integers! *)
let facto (Int -> Int)
 | 0 | 1 -> 1
 | n -> n * (facto (n - 1))
in
facto 300

(* The tail-recursive way *)
let facto ((Int,Int) -> Int)
 | (x, 0 | 1) -> x
 | (x, n) -> facto (x * n, n - 1)
in
facto (1,10000)
";"sumtype","type Expr = 
    (`add, Expr, Expr)
  | (`mul, Expr, Expr)
  | (`sub, Expr, Expr)
  | (`div, Expr, Expr)
  | Int
 
let eval ( Expr -> Int )  
  | (`add,x,y) -> eval x + eval y
  | (`mul,x,y) -> eval x * eval y
  | (`sub,x,y) -> eval x - eval y
  | (`div,x,y) -> (eval x) div (eval y)
  | n -> n 
in
eval (`add, 10, (`mul, 20, 5))
";"ovfun","type Person = FPerson | MPerson 
type FPerson = <person gender = \"F\" >[ Name Children (Tel | Email)?] 
type MPerson = <person gender=\"M\">[ Name Children (Tel | Email)?] 
type Children = <children>[Person*] 
type Name = <name>[ PCDATA ]
type Tel = <tel kind=?\"home\"|\"work\">['0'--'9'+ '-' '0'--'9'+]
type Email = <email>[PCDATA '@' PCDATA]

type Man = <man name=String>[ Sons Daughters ]
type Woman = <woman name=String>[ Sons Daughters ]
type Sons = <sons>[ Man* ]
type Daughters = <daughters>[ Woman* ]

let split (MPerson -> Man ; FPerson -> Woman)
  <person gender=g>[ <name>n <children>[(mc::MPerson | fc::FPerson)*]; _] ->
     let tag = match g with \"F\" -> `woman | \"M\" -> `man in
     let s = map mc with x -> split x in
     let d = map fc with x -> split x in
     <(tag) name=n>[ <sons>s  <daughters>d ] 
 

let base : Person = 
<person gender=\"F\">[ 
  <name>\"Themis\"
  <children>[ 
    <person gender=\"M\">[
      <name>\"Prometheus\"
      <children>[
        <person gender=\"M\">[
          <name>\"Deucalion\"
          <children>[]
        ]
      ]
      <email>\"focifero@olympus.com\"
    ] 
    <person gender=\"M\">[
      <name>\"Epimetheus\"
      <children>[]
      <tel> \"314-1592654\"
    ]
  ] 
  <tel kind=\"home\"> \"271-828182\"
]
in
split base
";"note","type Doc = <doc>Text
type Text = [ (Char | (Letter+ ' '* Note))* ]
type Letter = 'a'--'z' | 'A'--'Z'
type Note = <note>[ PCDATA ]

type Flow = [ (Char | <ref no=Int>[ PCDATA ])* ]
type Notes = [ <note no=Int>[ PCDATA ]* ]
type Result = <doc>[ <body>Flow <notes>Notes ]

let format (<doc>s : Doc) : Result = 
  let (body,notes) = text (s,1) in
  <doc>[ <body>body <notes>notes ]

let text ( (Text,Int) -> (Flow,Notes) )
 | ([ pre::Char*? (word::Letter+ ' '* <note>n); rem ], count) ->
      let (body,notes) = text (rem, count + 1) in
      (pre @ [<ref no=count>word] @ body, 
       [<note no=count>n] @ notes)
 | (body,_) -> (body, [])

let src : Doc = <doc>[ 'CDuce ' <note>\"Frisch, Castagna, Benzaken\"
		 ' is an XML ' <note>\"a W3C standard\"
		 '-friendly programming language.' ]
in
format src
";"biblio","type Biblio  = <bibliography>[Heading Paper*]
type Heading = <heading>[ PCDATA ]
type Paper   = <paper>[ Author+ Title Conference File ]
type Author  = <author>[ PCDATA ]
type Title   = <title>[ PCDATA ]
type Conference = <conference>[ PCDATA ]
type File    = <file>[ PCDATA ]

(* Simplified HTML *)
type Html  = <html>[ <head>[ <title>[ PCDATA ] ]  <body>Mix ]
type Mix   = [ ( <h1>Mix | <a href=String>Mix | <p>Mix | <em>Mix 
	       | <ul>[ <li>Mix +] | Char )* ]

let do_authors ([Author+] -> Mix)
 | [ <author>a ] -> a
 | [ <author>a <author>b ] -> a @ \" and, \" @ b
 | [ <author>a; x] -> a @ \", \" @ (do_authors x)

let do_paper (Paper -> <li>Mix)
  <paper>[ x::_* <title>t <conference>c <file>f ] ->
    <li>[ <a href=f>t !(do_authors x) '; in ' <em>c '.' ]

let do_biblio (Biblio -> Html)
  <bibliography>[ <heading>h; p ] ->
      let body = match p with
      | [] -> \"Empty bibliography\"
      | l -> [ <h1>h <ul>(map l with x -> do_paper x) ]
      in    
      <html>[ <head>[ <title>h ] <body>body ]

let bib : Biblio = 
  <bibliography>[
    <heading>\"Alain Frisch's bibliography\"
    <paper>[
      <author>\"Alain Frisch\"
      <author>\"Giuseppe Castagna\"
      <author>\"V�ronique Benzaken\"
      <title>\"Semantic subtyping\"
      <conference>\"LICS 02\"
      <file>\"semsub.ps.gz\"
    ]
    <paper>[
      <author>\"Mariangiola Dezani-Ciancaglini\"
      <author>\"Alain Frisch\"
      <author>\"Elio Giovannetti\"
      <author>\"Yoko Motohama\"
      <title>\"The Relevance of Semantic Subtyping\"
      <conference>\"ITRS'02\"
      <file>\"itrs02.ps.gz\"
    ]
    <paper>[
      <author>\"V�ronique Benzaken\"
      <author>\"Giuseppe Castagna\"
      <author>\"Alain Frisch\"
      <title>\"CDuce: a white-paper\"
      <conference>\"PLANX-02\"
      <file>\"planx.ps.gz\"
    ]
 ]
in
do_biblio bib
";"projection","(* The projection  e/t   is translated to:
   transform e with [ (x::t|_)* ]  -> x *)

type Biblio  = <bibliography>[Heading Paper*]
type Heading = <heading>[ PCDATA ]
type Paper   = <paper>[ Author+ Title Conference File ]
type Author  = <author>[ PCDATA ]
type Title   = <title>[ PCDATA ]
type Conference = <conference>[ PCDATA ]
type File    = <file>[ PCDATA ]

let bib : Biblio = 
  <bibliography>[
    <heading>\"Alain Frisch's bibliography\"
    <paper>[
      <author>\"Alain Frisch\"
      <author>\"Giuseppe Castagna\"
      <author>\"V�ronique Benzaken\"
      <title>\"Semantic subtyping\"
      <conference>\"LICS 02\"
      <file>\"semsub.ps.gz\"
    ]
    <paper>[
      <author>\"Mariangiola Dezani-Ciancaglini\"
      <author>\"Alain Frisch\"
      <author>\"Elio Giovannetti\"
      <author>\"Yoko Motohama\"
      <title>\"The Relevance of Semantic Subtyping\"
      <conference>\"ITRS'02\"
      <file>\"itrs02.ps.gz\"
    ]
    <paper>[
      <author>\"V�ronique Benzaken\"
      <author>\"Giuseppe Castagna\"
      <author>\"Alain Frisch\"
      <title>\"CDuce: a white-paper\"
      <conference>\"PLANX-02\"
      <file>\"planx.ps.gz\"
    ]
 ]

let titles = [bib]/<paper>_/<title>_
let authors = [bib]/<paper>_/<author>_
let titles_concat = [bib]/<paper>_/<title>_/Char
";"xtransform","(* For the purpose of the example we can consider this hugely
   simplified definition of Xhtml
*)

type Flow = Char | Block | Inline  ;;
type Block = P | Heading | Lists | Blocktext | Char
type Lists = Ul
type Blocktext = Pre |  Address | Center;;
type Inline = Char | A | Fontstyle
type Fontstyle = Tt | I | B | Big | Small;;

type Xhtml = <html>[ Head Body ];;
type Head = <head>[ Title <link>[ ]];;
type Title = <title>[ PCDATA ];;
type Body = <body bgcolor=?String>[ Block* ];;

type P = <p>[ Inline* ];;
type Heading = <(`h1 | `h2 | `h3 | `h4)>[ Inline* ];;

type Ul = <ul>[Li+];;
type Li = <li>[ Flow* ];;

type Address = <address>[ Inline* ];;
type Pre = <pre>[ (PCDATA | A | Fontstyle)* ];;
type Center = <center>[ Block* ];;

type A = <a ({ name = String } | { href = String })>[ (Inline)* ];;
type Tt = <tt>[ Inline* ];;
type I = <i>[ Inline* ];;
type B = <b>[ Inline* ];;
type Big = <big>[ Inline* ];;
type Small = <small>[ Inline* ];;


(* xtransform matches the patterns against the root element of each
   XML tree and, if it fails, it recursively applies itself to the
   sequence of sons of the root.

   It can be used to put in boldface all the links of an XHTML
   document as follows
*)

let bold (x:[Xhtml]):[Xhtml] = xtransform x with <a (y)>t -> [ <a (y)>[<b>t] ]


(* let us apply the function to a document where links appear
   at different depths
*)


let doc : Xhtml =
  <html>[
    <head>[<title>\"Example\" <link>[]]
    <body>[
      <h2>['You can have links ' <a href=\"here\">\"here\"]
      <pre>['Or they can be down']
      <ul>[
        <li>['In ' <a name=\"list\">\"lists\" ' for instance']
	<li>['or you oddly decided to ' 
             <center>[<p>[<a href=\"what?\">\"center\"]] 
             ' them '
            ]
      ]
      <address>[
        'and even if they are in fancy ' <a name=\"address\">\"address boxes\"
      ]
      <p>[
          'nevertheless ' <a href=\"http://www.cduce.org\">\"Cduce\" ' and '
          <a href=\"xtransform\">[<tt>\"xtransform\"] 
          ' will put all links in bold so that when'
          ' you program your transformation you '
          <big>[<a name=\"\">\" don't \" ] ' have to worry about it'
     ]
   ]
  ];;

bold [doc];;

let [x] = bold [doc] in print_xml x;;
";"reference","(* In CDuce the expression  \"ref T exp\" returns a reference  *)
(* to the result of \"exp\" and has type \"ref T\" provided that *)
(* \"exp\" is of type \"T\". References come equipped with three *)
(* operators: \":=\" (assignment), \"!\" (dereferencing), and \";\"*) 
(* (sequencing).                                             *)


let stack = ref [Int*] []

let fun push(x : Int) : []  = 
  stack := [x; !stack]

let fun pop ([] : []) : Int = 
  match !stack with [x; y] -> stack := y; x | _ -> raise \"Empty stack\"


(* In a pattern [ ... ; y] the variable y captures the tail  *)
(* of the sequence. It is equivalent to [ ... y::_*].        *)
(* In an expression [ ... ; e ] the expression e denotes the *)
(* tail of the sequence. It is equivalent to [ ... ] @ e     *)


;;

push 1;;
push 2;;
push 3;;
pop [];;
pop [];;
pop [];;
pop [];;
";"pm_compil","(* This example demonstrates the efficient compilation of pattern
   matching. *)

type A = <a>[ Int* ]
type B = <b>[ Char* ]

(* Imagine we want to compile the following function:
   fun ([A+|B+] -> Bool) [A+] -> 0 | [B+] -> 1

   For an arbitrary value, it is expensive to check whether it has
   type [A+] or not. But if we know statically that it has type [A+|B+],
   we just have to check the tag of the first element !

   This is demonstrated by the following internal debugging feature.
   The syntax is:  

   debug compile T P1 ... Pn

   where T is the input type (static information about the matched
   value) and P1,...,Pn are the patterns to compile (simultaneously).

   The \"debug compile\" instruction displays an human-readable
   representation of the automaton corresponding to the pattern
   matching. Note: in actual evaluation, this automaton is build
   lazily (= on-the-fly, = JIT).
*)

debug compile [A+|B+] [A+] [B+]


(* You can see on the output that the pattern matching is actually
   compiled as:

   fun ([A+|B+] -> Int) [ <a>_ ; _ ] -> 0 | _ -> 1
*)
"; ]
let present = "<ul><li><a href=\"/cgi-bin/cduce?example=q1\">Q1.</a> 
Q1 Query
</li><li><a href=\"/cgi-bin/cduce?example=q2\">Q2.</a> 
Q2 Query
</li><li><a href=\"/cgi-bin/cduce?example=q3\">Q3.</a> 
Q3 Query
</li><li><a href=\"/cgi-bin/cduce?example=q4\">Q4.</a> 
Q4 Query
</li><li><a href=\"/cgi-bin/cduce?example=q5\">Q5.</a> 
Q5 Query
</li><li><a href=\"/cgi-bin/cduce?example=q6\">Q6.</a> 
Q6 Query
</li><li><a href=\"/cgi-bin/cduce?example=q8\">Q8.</a> 
Q8 Query
</li><li><a href=\"/cgi-bin/cduce?example=q9\">Q9.</a> 
Q9 Query
</li><li><a href=\"/cgi-bin/cduce?example=q11\">Q11.</a> 
Q11 Query
</li><li><a href=\"/cgi-bin/cduce?example=q12\">Q12.</a> 
Q12 Query
</li><li><a href=\"/cgi-bin/cduce?example=qatt1\">Attributs et �l�ments.</a> 
Partie 1
</li><li><a href=\"/cgi-bin/cduce?example=qatt2\">Attributs et �l�ments.</a> 
Partie 2
</li><li><a href=\"/cgi-bin/cduce?example=qatt3\">Attributs et �l�ments.</a> 
Partie 3
</li><li><a href=\"/cgi-bin/cduce?example=xml\">XML elements.</a> 
XML elements.
</li><li><a href=\"/cgi-bin/cduce?example=functions\">Functions.</a> 
Several syntaxes to define functions.
</li><li><a href=\"/cgi-bin/cduce?example=mutrec\">Mutual recursion.</a> 
Mutual toplevel definition for types and functions.
</li><li><a href=\"/cgi-bin/cduce?example=sequence\">Sequence literals.</a> 
How to write sequences.
</li><li><a href=\"/cgi-bin/cduce?example=seqtypes\">Sequence types.</a> 
Types for sequences.
</li><li><a href=\"/cgi-bin/cduce?example=integers\">The factorial function.</a> 
What about computing 10000! ?
</li><li><a href=\"/cgi-bin/cduce?example=sumtype\">Sum types.</a> 
How to simulate ML sum types.
</li><li><a href=\"/cgi-bin/cduce?example=ovfun\">Overloaded functions.</a> 
This examples demonstrates the use of overloaded functions.
</li><li><a href=\"/cgi-bin/cduce?example=note\">Footnotes.</a> 
 This example shows how to bind an XML element with surrounding text.
</li><li><a href=\"/cgi-bin/cduce?example=biblio\">Bibliography.</a> 
The good old XML bibliography example.
</li><li><a href=\"/cgi-bin/cduce?example=projection\">Projection.</a> 
Syntactic sugar for projection.
</li><li><a href=\"/cgi-bin/cduce?example=xtransform\">Tree transformations.</a> 
How to perform XSLT-like transformations.
</li><li><a href=\"/cgi-bin/cduce?example=reference\">References.</a> 
Mutable values.
</li><li><a href=\"/cgi-bin/cduce?example=pm_compil\">Compilation of pattern matching.</a> 
This example demonstrates the efficient compilation of pattern
matching.
</li></ul>"