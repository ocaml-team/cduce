let features = ref []

let init_all () = List.iter (fun (_,d,f) -> f()) (List.rev !features)
let register n d f = features := (n,d,f) :: !features
let descrs () = List.rev_map (fun (n,d,_) -> (n,d)) !features
let inhibit n = features := List.filter (fun (n',_,_) -> n <> n') !features

