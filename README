README - CDuce

This package is distributed under the terms of an MIT-like License
(see the LICENSE file).


------------------------------------------------------------------------------
Abstract
------------------------------------------------------------------------------

CDuce is a programming language adapted to the manipulation of XML
documents.  It has been designed and developed as a joint research
project between the Languages group at ENS (Paris) and the Database
group at LRI (Orsay) (http://www.lri.fr/bd).

The current implementation has been written to demonstrate the
innovative features of the language and to validate design choices.
It is a research prototype, not a robust application. However, it has 
been reported to work properly in a production environment.

Further information about CDuce is available at

  http://www.cduce.org


------------------------------------------------------------------------------
Source distribution
------------------------------------------------------------------------------

See the INSTALL file for Installation instructions.

1. CDuce command-line interpreter / toplevel

For performance reasons, it is advised to build it using OCaml native code
compiler (by default in the Makefile).

See the man page or HTML manual for usage.

When no CDuce file is given on the command line, the interpreter
behaves as a toplevel. Phrases are interpreted when the user type
";;". Of course, mutually recursive definition (types or functions)
must be entirely contained in an adjacent sequence of phrases
(without ";;" inbetween).


2. CDuce CGI interface

It is also possible to play with CDuce in a web browser, using the
webiface CGI interface (or webiface.opt) program. Of course, You will
need a web server with CGI support. 

There is usually a running version at:

http://www.cduce.org/cgi-bin/cduce

Some features are disabled in the web interface (including
file manipulation).


3. dtd2cduce

dtd2cduce is small tool that generates CDuce type declarations from
a DTD.

Usage: dtd2cduce <prefix> <.dtd file>

<prefix> is a string prepended to tag names to form CDuce type names.
The generated declarations are sent to the standard output.

dtd2cduce can also operates as a cgi-bin. You can access it using
the form at this URL:

http://www.cduce.org/dtd2cduce.html


4. CDuce web site

The source files (XML + CDuce) for the CDuce web site
(http://www.cduce.org/) are included in the web/ subdirectory.

------------------------------------------------------------------------------
Binary distribution
------------------------------------------------------------------------------

If you have downloaded the "linux static" binary distribution, you got
the cduce and dtd2cduce executables, the man pages, and the web/
subdirectory. To build the website, just do "make".


------------------------------------------------------------------------------
Documentation
------------------------------------------------------------------------------

There are several sources of information concerning CDuce:

- we started to write a manual and a tutorial as part of the web site. 
  You can find the manual at:

  http://www.cduce.org/manual.html

  or generate it locally (make local_website). Note that the online
  version is probably more up-to-date than the one included in the
  distribution.


- papers about CDuce:

  http://www.cduce.org/papers.html


- code examples:

  In the web site:
  http://www.cduce.org/examples.html
  http://www.cduce.org/bench.html
  
  In the distribution: 
  web/site.cd
  web/examples/build.cd

  Online prototype:
  http://www.cduce.org/cgi-bin/cduce

- the CDuce users' mailing-list:
  http://www.cduce.org/mailing.html


- and of course the source code!


------------------------------------------------------------------------------
Feedback
------------------------------------------------------------------------------

We'll appreciate any feedback (comments, suggestions, questions,
bug reports, ...) concerning the CDuce language or this early release
of the interpreter at info@cduce.org



  -- The CDuce team
