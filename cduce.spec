Summary: a language for fast and type safe processing of XML documents.
Name: cduce
Version: 0.4.0
Release: 1
URL: http://www.cduce.org/
Source: cduce-%{version}.tar.gz
License: MIT
Group: Development/Languages
BuildRoot: %{_tmppath}/%{name}-root
Requires: pcre >= 4.5

%description

CDuce is a functional language for fast and type safe
processing  of XML documents.

A  complete  documentation,  reference  manual,  tutorial,
technical   articles  on  implementation  and  theoretical
issues, benchmarks, source code, mailing lists and  forums
can be found on the CDuce website: http://www.cduce.org.

%prep
%setup -D -q
cat > find_requires.sh << EOF
#!/bin/sh
/usr/lib/rpm/find-requires | fgrep -v libpcre.so.3
exit 0
EOF
chmod +x find_requires.sh

%define __find_requires %_builddir/%name-%version/find_requires.sh

%build
./configure --without-expat --without-curl --with-ocamlopt
make 

%install
rm -rf %{buildroot}
make install_bin BINDIR=%{buildroot}%{_bindir}
make install_doc MANDIR=%{buildroot}%{_mandir} DOCDIR=%{buildroot}%{_docdir}/cduce

%post
[ -a /lib/libpcre.so.3 -o -a /usr/lib/libpcre.so.3 ] || {
  { { LIBPCRE0=/lib/libpcre.so.0 && [ -a $LIBPCRE0 ]; } ||
    { LIBPCRE0=/usr/lib/libpcre.so.0 && [ -a $LIBPCRE0 ]; }
  } && ln -s $LIBPCRE0 /usr/lib/libpcre.so.3
}

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%{_docdir}/cduce
%{_bindir}/cduce
%{_bindir}/dtd2cduce
%{_mandir}/man1/*cduce.1*

%changelog
* Fri Apr  7 2006 Nils Gesbert <nils.gesbert@ens.fr> 0.4.0
  Updated the spec file for cduce 0.4.0

* Mon Sep  1 2003 Giuseppe Castagna <Giuseppe.Castagna@ens.fr>
0.1.1
  * Various bug fixes  (expat might now work)
  * Sequencing operator   e1;e2  (equivalent to: let [] = e1 in e2)
  * Encoded references

* Fri Jul  4 2003 Giuseppe Castagna <Giuseppe.Castagna@ens.fr>
0.1.0
  * Support for XML Namespaces
  * Better support for expat; clean Makefile
  * Get rid of ;; and let fun in examples
  * Optional ; for record in attribute position (values and types/patterns)
  * Split --dump into --load and --save
  * Better handling of strings (specific node in AST)
  * Map, transform: tail-recursive implementation
  * Preliminary support for XML Schema
  * Various bug fixes



