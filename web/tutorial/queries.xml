<?xml version="1.0" encoding="ISO-8859-1" standalone="yes"?>
<page name="tutorial_queries">

<title>Queries</title>
<left>
<boxes-toc/>
<p>
You can cut and paste the code on this page and 
test it on the <a href="cgi-bin/cduce">online interpreter</a>.
</p>
</left>


<box title="Select from where" link="sel">

<b style="color:#FF0080">WE SHOULD REWRITE THIS SECTION TO
GET RID OF THE MANY TYPE DEFINITIONS. JUST USE THE ONE 
OF USE CASES</b>


<p>
CDuce is endowed with a <code>select_from_where</code> syntax to perform some SQL-like queries. The general form of select expressions is
</p>
<sample><![CDATA[
select %%e%% from
   %%p1%% in %%e1%%,
   %%p2%% in %%e2%%,
       :
   %%pn%% in %%en%%
where %%b%%
]]></sample>
<p>
where <code>%%e%%</code> is an expression <code>%%b%%</code> a boolean expression, the <code>%%pi%%</code>'s are patterns, and  the <code>%%ei%%</code>'s are sequence expressions.
</p>

<p>
The <code>select_from_where</code> construction is translated into:
</p>
<sample><![CDATA[
transform %%e1%% with %%p1%% -> 
   transform %%e2%% with %%p2%% -> 
         ...
       transform %%en%% with %%pn%% -> 
          if %%b%% then  [%%e%%] else []
]]>
</sample>


</box>

<box title="XPath-like expressions" link="xpath">
<p> XPath-like expressions are of two kind :
<code> %%e%%/%%t%% </code>, <code> %%e%%/@%%a%% </code>, (and  <code> %%e%%//%%t%% </code>) where <code>%%e%%</code> is an expression, <code> %%t%% </code> a type, and <code> %%a%% </code> an attribute.
</p>

<p> 
They are syntactic sugar for :</p> 
<sample> <![CDATA[flatten(select x from <_ ..>[(x::t | _ )*] in e)]]> </sample>
<p>  and </p>
<sample><![CDATA[
select x from <_ a=x ..>_ in  e]]>
</sample>
</box>

<box title="Examples" link="exsel">
<section title="Types and data for the examples">

<p> Let us consider the following DTD and the CDuce types representing a Bibliography </p>
<sample><![CDATA[
<!ELEMENT bib  (book* )>
<!ELEMENT book  (title,  (author+ | editor+ ), publisher, price )>
<!ATTLIST book  year CDATA  #REQUIRED >
<!ELEMENT author  (last, first )>
<!ELEMENT editor  (last, first, affiliation )>
<!ELEMENT title  (#PCDATA )>
<!ELEMENT last  (#PCDATA )>
<!ELEMENT first  (#PCDATA )>
<!ELEMENT affiliation  (#PCDATA )>
<!ELEMENT publisher  (#PCDATA )>
<!ELEMENT price  (#PCDATA )>

type bib = <bib>[book*]
type book = <book year=String>[title  (author+ | editor+ ) publisher price ]
type author = <author>[last  first ]
type editor = <editor>[last  first  affiliation ]
type title  = <title>[PCDATA ]
type last  = <last>[PCDATA]
type first  = <first>[PCDATA]
type affiliation = <affiliation>[PCDATA]
type publisher  = <publisher>[PCDATA]
type price  = <price>[PCDATA]
]]>
</sample>
<p> and some values </p>
<sample><![CDATA[
let biblio : bib = 
           <bib>[
              <book year="1994">[
                 <title>['TCP/IP Illustrated']
                 <author>[
	            <last>['Stevens']
		    <first>['W.']]
                 <publisher>['Addison-Wesley']
	         <price>['65.95'] ]
	     <book year="1992">[
                <title>['Advanced Programming in the Unix environment']
		<author>[
		   <last>['Stevens'] 
		   <first>['W.']]
        	<publisher>['Addison-Wesley']
        	<price>['65.95'] ]
	     <book year="2000">[ 
	        <title>['Data on the Web']
        	<author>[
		  <last>['Abiteboul'] 
		  <first>['Serge']]
	        <author>[
		  <last>['Buneman'] 
		  <first>['Peter']]
                <author>[
		  <last>['Suciu'] 
		  <first>['Dan']]
	        <publisher>['Morgan Kaufmann Publishers']
                <price>['39.95'] ]
	     <book year="1999">[
	        <title>['The Economics of Technology and Content for Digital TV']
                <editor>[
                   <last>['Gerbarg'] 
		   <first>['Darcy']
                   <affiliation>['CITI'] ]
		<publisher>['Kluwer Academic Publishers']
                <price>['129.95']]
             ]
]]>
</sample>
</section>


<section title="Projections">
<ul>
<li> All titles in the bibliography biblio 
<sample><![CDATA[
let titles = [biblio]/book/title
]]>
</sample>
<p> Which yields to:</p>
<sample><![CDATA[
val titles : [ title* ] = [ <title>[ 'TCP/IP Illustrated' ]
                            <title>[ 'Advanced Programming in the Unix environment' ]
                            <title>[ 'Data on the Web' ]
                            <title>[ 'The Economics of Technology and Content for Digital TV' ]
                          ]
]]>
</sample>

</li>
 <li>All authors in the bibliography biblio
<sample><![CDATA[
let authors = [biblio]/book/<author>_
]]>
</sample>
<p> Yielding the result: </p>
<sample><![CDATA[
val authors : [ <author>[ last first ]* ] = 
                            [ <author>[ <last>[ 'Stevens' ] <first>[ 'W.' ] ]
                              <author>[ <last>[ 'Stevens' ] <first>[ 'W.' ] ]
                              <author>[
                                <last>[ 'Abiteboul' ]
                                <first>[ 'Serge' ] ]
                              <author>[
                                <last>[ 'Buneman' ]
                                <first>[ 'Peter' ] ]
                              <author>[ <last>[ 'Suciu' ] <first>[ 'Dan' ] ]
                            ]
]]>
</sample>
<p>Note the difference between this two projections.<br/> 
In the fist one, we use the preset type title (<![CDATA[type title  = <title>[PCDATA ]]]>
).<br/>
In the second one, the type <![CDATA[<author>_]]> means all the xml fragments beginning by the tag author ( _ means Any), 
and this tag is without attribute. In contrary, we write note  <![CDATA[<author ..>_]]>. 
</p>
</li>
<li> All books having an editor in the bibliography biblio 
<sample><![CDATA[
let edibooks = [biblio]/<book year=_>[_* editor _*]
]]>
</sample>
<p> Yielding: </p>
<sample><![CDATA[
val edibooks : [ <book year=String>[ title editor+ publisher price]* ] = 
           [ <book year="1999">[
                 <title>[ 'The Economics of Technology and Content for Digital TV']
                 <editor>[
                    <last>[ 'Gerbarg' ]
                    <first>[ 'Darcy' ]
                 <affiliation>[ 'CITI' ] ]
                 <publisher>[ 'Kluwer Academic Publishers' ]
                 <price>[ '129.95' ] ]
          ]
]]>
</sample>
</li>
<li> All books without authors. 
<sample><![CDATA[
let edibooks2 = [biblio]/<book ..>[(Any\author)*]
]]>
</sample>
<p> Yielding: </p>
<sample><![CDATA[
val edibooks2 : [ <book year=String>[ title editor+ publisher price]* ] = 
           [ <book year="1999">[
                 <title>[ 'The Economics of Technology and Content for Digital TV']
                 <editor>[
                    <last>[ 'Gerbarg' ]
                    <first>[ 'Darcy' ]
                 <affiliation>[ 'CITI' ] ]
                 <publisher>[ 'Kluwer Academic Publishers' ]
                 <price>[ '129.95' ] ]
          ]
]]>
</sample>
</li>
<li> The year of books having a price of 65.95 in the bibliography biblio 
<sample><![CDATA[
let books = [biblio]/<book ..>[_* <price>['65.95']]/@year
]]>
</sample>
<p> Yielding: </p>
<sample><![CDATA[
val books : [ String* ] = [ "1994" "1992" ]
]]>
</sample>
</li>
<li> All the authors and editors in biblio
<sample><![CDATA[
let aebooks = [biblio]/book/(author|editor)
]]>
</sample>
<p> Yielding: </p>
<sample><![CDATA[
val aebooks : [ (editor | author)* ] = [ <author>[
                                         <last>[ 'Stevens' ]
                                         <first>[ 'W.' ]
                                         ]
                                       <author>[
                                         <last>[ 'Stevens' ]
                                         <first>[ 'W.' ]
                                         ]
                                       <author>[
                                         <last>[ 'Abiteboul' ]
                                         <first>[ 'Serge' ]
                                         ]
                                       <author>[
                                         <last>[ 'Buneman' ]
                                         <first>[ 'Peter' ]
                                         ]
                                       <author>[
                                         <last>[ 'Suciu' ]
                                         <first>[ 'Dan' ]
                                         ]
                                       <editor>[
                                         <last>[ 'Gerbarg' ]
                                         <first>[ 'Darcy' ]
                                         <affiliation>[ 'CITI' ]
                                         ]
                                       ]
]]>
</sample>
</li>
</ul>
<p>
An interesting point in Cduce is the static typing of an expression. By example if we consider the third projection, 
"All books having an editor in the bibliography", CDuce knows the type of the result of the value <code>%%edibooks%%</code>:
</p><sample><![CDATA[
val edibooks : [ <book year=String>[ title editor+ publisher price]* ] = 
...
]]>
</sample>
<p>This type represents a book without author (see the book type in the type declaration in the top of this section).
Now if we want to know all authors of this list of books <code>%%edibooks%%</code>:
</p>
<sample><![CDATA[
let authorsofedibooks = edibooks/author
]]>
</sample>
<p>Yelding:</p>
<sample><![CDATA[
Warning at chars 24-39:
This projection always returns the empty sequence
val authorsofedibooks : [  ] = ""
]]>
</sample>
<p>
In fact the value <code>%%edibooks%%</code> must be a subtype of <![CDATA[[<_ ..>[Any* author Any*] *]]]>, 
and here this is not the case. 
If you want to be sure, test this:
</p>
<sample><![CDATA[
match edibooks with [<_ ..>[_* author _*] *] -> "this is a subtype" | _ -> "this is not a subtype" ;;
]]>
</sample>
<p>
An other projection should be convince you, is the query:
</p>
<sample><![CDATA[
let freebooks = [biblio]/book/<price>['0']
]]]>
</sample>
<p>Yelding:</p>
<sample><![CDATA[
val freebooks : [ <price>[ '0' ]* ] = ""
]]>
</sample>
<p>
There is no free books in this bibliography, That is not indicated by the type of biblio.
Then, this projection returns the empty sequence (<code>""</code>)</p>
</section>



<section title="Select_from_where">
<p>The same queries we wrote above can of course be programmed with the <code>select_from_where</code> construction </p>
<p> All the titles </p>
<sample><![CDATA[
let tquery = select y 
             from x in [biblio]/book ,
                  y in [x]/title
]]>
</sample>
<p> This query is programmed in a XQuery-like style largely relying on the projections. Note that <code>x</code> and <code>y</code> are CDuce's patterns. The result is:</p>
<sample> <![CDATA[
val tquery : [ title* ] = [ <title>[ 'TCP/IP Illustrated' ]
                            <title>[ 'Advanced Programming in the Unix environment' ]
                            <title>[ 'Data on the Web' ]
                            <title>[ 'The Economics of Technology and Content for Digital TV' ]
                          ]
]]>
</sample>
<p> Now let's program the same query with the translation given previously thus eliminating the <code>y</code> variable </p>
<sample><![CDATA[
let withouty = flatten(select [x] from x in [biblio]/book/title)
]]>
</sample>

<p> Yielding: </p>
<sample><![CDATA[
val tquery : [ title* ] = [ <title>[ 'TCP/IP Illustrated' ]
                            <title>[ 'Advanced Programming in the Unix environment' ]
                            <title>[ 'Data on the Web' ]
                            <title>[ 'The Economics of Technology and Content for Digital TV' ]
                          ]
]]>
</sample>



<p>
But the <code>select_from_where</code> expressions are likely to be used for
more complex queries such as the one that selects all titles whose at least one
author is "Peter Buneman" or "Dan Suciu"
</p>
<sample><![CDATA[
let sel = select y 
          from x in [biblio]/book ,
               y in [x]/title,
               z in [x]/author
where ( (z = <author>[<last>['Buneman']<first>['Peter']])
 || (z = <author>[<last>['Suciu'] <first>['Dan']]) )
]]>
</sample>
<p> Which yields: </p>

<sample><![CDATA[
val sel : [ title* ] = [ <title>[ 'Data on the Web' ]
                         <title>[ 'Data on the Web' ]
                         ]
]]>
</sample>

<p>Note that the corresponding semantics, as in SQL, is a multiset one.
Thus duplicates are not eliminated. To discard them, one has to use the <code>distinct_values</code> operator.
</p>
</section>

<section title="A pure pattern example">
<p>
This example computes the same result as the previous query except that
duplicates are eliminated. It is written in a pure pattern form (i.e., without
any XPath-like projections)
</p>

<sample><![CDATA[
let sel = select t
from <_ ..>[(x::book| _ )*] in [biblio],
     <_ ..>[ t&title _* (<author>[<last>['Buneman']<first>['Peter']]
                       | <author>[<last>['Suciu'] <first>['Dan']]) ; _]  in x  

]]>
</sample>
<p>
Note the pattern on the second line in the <code> from </code> clause. 
As the type of an element in <code>x</code> is 
<code><![CDATA[type book = <book year=String>[title  (author+ | editor+ ) publisher price ]]]></code>, 
we skip the tag : 
<code><![CDATA[<_ ..>]]></code>, then  
we then capture the corresponding title <code><![CDATA[(t &title)]]></code>
then we skip authors <code><![CDATA[_*]]> </code> 
until we find either Peter Buneman or Dan Suciu 
<code><![CDATA[ (<author>[<last>['Buneman']<first>['Peter']]| <author>[<last>['Suciu'] <first>['Dan']])]]></code>,
then we skip the remaining authors and the tail of the sequence by writing <code>; _</code>
</p>
<p>
Result:
</p>
<sample><![CDATA[
val sel : [ title* ] = [ <title>[ 'Data on the Web' ] ]]]>
</sample>
<p>
This pure pattern form of the query yields (in general) better performance than
the same one written in an XQuery-like programming style. However, the query
optimiser automatically translates the latter into a pure pattern one
</p>
</section>
<section title="Joins">


<p>
This example is the exact transcription of <a href="http://www.w3.org/TR/xquery-use-cases/#xmp-queries-results-q5">query Q5 of XQuery use cases</a>.
On top of this section we give the corresponding CDuce types.
We give here the type of the document to be joined, and the sample value.
</p>

<sample><![CDATA[
type Reviews =<reviews>[Entry*]
type Entry = <entry> [ Title Price Review]
type Title = <title>[PCDATA]
type Price= <price>[PCDATA]
type Review =<review>[PCDATA]

let bstore2 : Reviews =
<reviews>[
    <entry>[
        <title>['Data on the Web']
        <price>['34.95']
        <review>
               ['A very good discussion of semi-structured database
               systems and XML.']
     ]
    <entry>[
        <title>['Advanced Programming in the Unix environment']
        <price>['65.95']
        <review>
               ['A clear and detailed discussion of UNIX programming.']
     ]
    <entry>[
        <title>['TCP/IP Illustrated']
        <price>['65.95']
        <review>
               ['One of the best books on TCP/IP.']
    ]
]
]]>
</sample>
<p>

The queries are expressed first in an XQuery-like style, then in a pure pattern style: the first pattern-based query is the one produced by the automatic translation from the first one. The last query correponds to a pattern aware programmer's version.
</p>

<p>
XQuery style
</p>
<sample><![CDATA[
<books-with-prices>
select <book-with-price>[t1 <price-bstore1>([p1]/Char) <price-bstore2>([p2]/Char)]
from b in [biblio]/book ,
     t1 in [b]/title,
     e in [bstore2]/Entry,
     t2 in [e]/Title,
     p2 in [e]/Price,
     p1 in [b]/price
 where t1=t2
]]>
</sample>

<p> Automatic translation of the previous query into a pure pattern (thus more efficient) one </p>
<sample><![CDATA[
<books-with-prices>
select <book-with-price>[t1 <price-bstore1>x10 <price-bstore2>x11 ]
from <_ ..>[(x3::book|_)*] in [biblio],
     <_ ..>[(x9::price|x5::title|_)*] in x3,
     t1 in x5,
     <_ ..>[(x6::Entry|_)*] in [bstore2],
     <_ ..>[(x7::Title|x8::Price|_)*] in x6,
     t2 in x7,
     <_ ..>[(x10::Char)*] in x9,
     <_ ..>[(x11::Char)*] in x8
 where t1=t2
]]>
</sample>

<p>  
Pattern aware programmer's version of the same query (hence hand optimised).
This version of the query is very efficient. Be aware of patterns.
</p>

<sample><![CDATA[
<books-with-prices>
select <book-with-price>[t2 <price-bstore1>p1 <price-bstore2>p2]
from <bib>[b::book*] in [biblio],
     <book ..>[t1&title _* <price>p1] in b,
     <reviews>[e::Entry*] in [bstore2],
     <entry>[t2&Title <price>p2 ;_] in e
where t1=t2
]]>
</sample>
</section>


<section title="More complex Queries: on the power of patterns">
<sample><![CDATA[
let biblio = [biblio]/book;;

<bib>
    select <book (a)> x
      from <book (a)>[ (x::(Any\editor)|_ )* ] in biblio 
]]>
</sample>

<p>
This expression returns all book  in bib but removoing the editor element.
If one wants to write more explicitly:
</p>
<sample><![CDATA[
    select <book (a)> x
      from <book (a)>[ (x::(Any\$$<editor ..>_$$)|_ )* ] in biblio
]]>
</sample>
<p>Or even:
</p>
<sample><![CDATA[
    select <book (a)> x
      from <book (a)>[ (x::(<($$_$$\$$`editor$$) ..>_)|_ )* ] in biblio
]]>
</sample>
<p>Back to the first one:</p>
<sample><![CDATA[
<bib>
    select <book (a)> x
      from <$$($$book$$)$$ (a)>[ (x::(Any\editor)|_ )* ] in biblio
]]>
</sample>
<p>
This query takes any element in bib, tranforms it in a book element and
removes sub-elements  editor, but you will get a warning as capture variable <code>book</code> in the <code>from</code> is never used: we should have written <code>&lt;(_) (a)></code> instead of <code>&lt;(book) (a)></code>
the from 
</p>
<sample><![CDATA[
    select <$$($$book$$)$$ (a)> x
      from <(book) (a)>[ (x::(Any\editor)|_ )* ] in biblio
]]>
</sample>
<p> Same thing but without tranforming tag to  "book".<br/>
More interestingly:</p>
<sample><![CDATA[
    select <(b) (a\$$id$$)> x
      from <(b) (a)>[ (x::(Any\editor)|_ )* ] in biblio
]]>
</sample>
<p>removes all "id" attribute (if any) from the attributes of the element in bib.
</p>
<sample><![CDATA[

    select <(b) (a\id+{bing=a.id})> x
      from <(b) (a)>[ (x::(Any\editor)|_ )* ] in biblio
]]>
</sample>
<p>Changes attribute <code>id=x</code> into <code>bing=x</code>
However, one must be sure that each element in <code>bib</code> has an <code>id</code> attribute
if such is not the case the expression is ill-typed. If one wants to perform this only for those elements which certainly have an <code>id</code> attribute then:
</p>
<sample><![CDATA[
    select <(b) (a\id+{bing=a.id})> x
      from <(b) (a&{id=_})>[ (x::(Any\editor)|_ )* ] in biblio
]]>
</sample>
</section>
<section title="An unorthodox query: Formatted table generation">

<p>
The following program generates a 10x10 multiplication table:
</p>

<sample><![CDATA[
let bg ((Int , Int) -> String) 
  (y, x) -> if (x mod 2 + y mod 2 <= 0) then "lightgreen" 
            else if (y mod 2 <= 0) then "yellow"
	    else if (x mod 2 <= 0) then "lightblue"
	    else "white";;

<table border="1">
  select <tr> select <td align="right" style=("background:"@bg(x,y)) >[ (x*y) ]
              from y in [1 2 3 4 5 6 7 8 9 10] : [1--10*] 
  from x in [1 2 3 4 5 6 7 8 9 10] : [1--10*];;
 ]]></sample>
<p>
The result is the xhtml code that generates the following table:
</p>
<table border="1"><col/><col/><col/><col/><col/><col/><col/><col/><col/><col/><tr><td align="right" style="background:white">1</td><td align="right" style="background:lightblue">2</td><td align="right" style="background:white">3</td><td align="right" style="background:lightblue">4</td><td align="right" style="background:white">5</td><td align="right" style="background:lightblue">6</td><td align="right" style="background:white">7</td><td align="right" style="background:lightblue">8</td><td align="right" style="background:white">9</td><td align="right" style="background:lightblue">10</td></tr><tr><td align="right" style="background:yellow">2</td><td align="right" style="background:lightgreen">4</td><td align="right" style="background:yellow">6</td><td align="right" style="background:lightgreen">8</td><td align="right" style="background:yellow">10</td><td align="right" style="background:lightgreen">12</td><td align="right" style="background:yellow">14</td><td align="right" style="background:lightgreen">16</td><td align="right" style="background:yellow">18</td><td align="right" style="background:lightgreen">20</td></tr><tr><td align="right" style="background:white">3</td><td align="right" style="background:lightblue">6</td><td align="right" style="background:white">9</td><td align="right" style="background:lightblue">12</td><td align="right" style="background:white">15</td><td align="right" style="background:lightblue">18</td><td align="right" style="background:white">21</td><td align="right" style="background:lightblue">24</td><td align="right" style="background:white">27</td><td align="right" style="background:lightblue">30</td></tr><tr><td align="right" style="background:yellow">4</td><td align="right" style="background:lightgreen">8</td><td align="right" style="background:yellow">12</td><td align="right" style="background:lightgreen">16</td><td align="right" style="background:yellow">20</td><td align="right" style="background:lightgreen">24</td><td align="right" style="background:yellow">28</td><td align="right" style="background:lightgreen">32</td><td align="right" style="background:yellow">36</td><td align="right" style="background:lightgreen">40</td></tr><tr><td align="right" style="background:white">5</td><td align="right" style="background:lightblue">10</td><td align="right" style="background:white">15</td><td align="right" style="background:lightblue">20</td><td align="right" style="background:white">25</td><td align="right" style="background:lightblue">30</td><td align="right" style="background:white">35</td><td align="right" style="background:lightblue">40</td><td align="right" style="background:white">45</td><td align="right" style="background:lightblue">50</td></tr><tr><td align="right" style="background:yellow">6</td><td align="right" style="background:lightgreen">12</td><td align="right" style="background:yellow">18</td><td align="right" style="background:lightgreen">24</td><td align="right" style="background:yellow">30</td><td align="right" style="background:lightgreen">36</td><td align="right" style="background:yellow">42</td><td align="right" style="background:lightgreen">48</td><td align="right" style="background:yellow">54</td><td align="right" style="background:lightgreen">60</td></tr><tr><td align="right" style="background:white">7</td><td align="right" style="background:lightblue">14</td><td align="right" style="background:white">21</td><td align="right" style="background:lightblue">28</td><td align="right" style="background:white">35</td><td align="right" style="background:lightblue">42</td><td align="right" style="background:white">49</td><td align="right" style="background:lightblue">56</td><td align="right" style="background:white">63</td><td align="right" style="background:lightblue">70</td></tr><tr><td align="right" style="background:yellow">8</td><td align="right" style="background:lightgreen">16</td><td align="right" style="background:yellow">24</td><td align="right" style="background:lightgreen">32</td><td align="right" style="background:yellow">40</td><td align="right" style="background:lightgreen">48</td><td align="right" style="background:yellow">56</td><td align="right" style="background:lightgreen">64</td><td align="right" style="background:yellow">72</td><td align="right" style="background:lightgreen">80</td></tr><tr><td align="right" style="background:white">9</td><td align="right" style="background:lightblue">18</td><td align="right" style="background:white">27</td><td align="right" style="background:lightblue">36</td><td align="right" style="background:white">45</td><td align="right" style="background:lightblue">54</td><td align="right" style="background:white">63</td><td align="right" style="background:lightblue">72</td><td align="right" style="background:white">81</td><td align="right" style="background:lightblue">90</td></tr><tr><td align="right" style="background:yellow">10</td><td align="right" style="background:lightgreen">20</td><td align="right" style="background:yellow">30</td><td align="right" style="background:lightgreen">40</td><td align="right" style="background:yellow">50</td><td align="right" style="background:lightgreen">60</td><td align="right" style="background:yellow">70</td><td align="right" style="background:lightgreen">80</td><td align="right" style="background:yellow">90</td><td align="right" style="background:lightgreen">100</td></tr></table>

</section>
</box>

</page>
