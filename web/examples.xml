<?xml version="1.0" encoding="ISO-8859-1" standalone="yes"?>
<page name="examples">

<title>Advanced examples</title>

<left>
<p>This page presents some sample CDuce programs.</p>
<boxes-toc/>
<p>See also:</p>
<local-links href="index,proto,memento"/>
</left>


<box title="Advanced examples" link="start">

This page presents some advanced programming examples in CDuce. <b
style="color:#FF0080">If you never saw CDuce programs before, this is the wrong
page</b> to start with. Rather follow our <local href="tutorial"/>, or test the
simple examples in our <a href="http://reglisse.ens.fr/cgi-bin/cduce">on line demo</a>.

</box>


<box title="Our canonical example" link="sort">
<p>
The example below is the one we use to demonstrate
how overloaded functions can avoid duplicating code.
Without overloaded functions, we would need to define
two mutually recursive functions in order to type-check
the transformation. Here, two constraints
in the (highlighted) function interface
can express precisely the behaviour of the function.
A detailed explanation of the code can be found <a href="tutorial_overloading.html#canonical">here</a>.
</p>
<sample>
<include-verbatim file="overloading.cd"/>
</sample>
</box>

<box title="Datatypes + first-class functions" link="data">
<p>
The program below shows how to simulate ML data types in CDuce.
It implements a (naive backtracking) regular expression recognizer.
The examples also demonstrate the use of first-class functions
(used as continuations).
</p>
<p>
Exercise for the reader: show that the algorithm may not terminate for 
some special regular expressions.
</p>
<sample highlight="false">
<include-verbatim file="regexp.cd"/>
</sample>
</box>

<box title="First-class functions and XML together" link="funxml">
<p>
The program below illustrates the use of first-class functions stored
in (pseudo) XML documents.
</p>
<sample highlight="false">
<include-verbatim file="funxml.cd"/>
</sample>
</box>

<box title="CDuce quine" link="quine">
<p>
A quine (a.k.a. self-rep) is a program that produces its own source
code. Here is an example of a quine written in CDuce.
</p>
<sample highlight="false">
<include-verbatim file="quine.cd"/>
</sample>
</box>

<box title="The script that generates this site" link="site">
<p>
The script below is one of the longest CDuce application ever written
;-) It is used to produce all the pages of this web site (except
the <local href="proto">web prototype</local> which is
a CGI script written in OCaml). CDuce type system ensures
that produced pages are valid w.r.t XHTML 1.0 Strict.
</p>
<p>
This program features both XML and text-content manipulation.
It also demonstrates the use of non-XML internal data structures.
Here, a tree represents internally the site
structure, and a list represents the path from the
root to the current page (in order to display the "You're here" line).
</p>
<sample highlight="false">
<include-verbatim file="site.cd"/>
</sample>
</box>

</page>

