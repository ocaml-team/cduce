#!/bin/sh

ROOT=$(git rev-parse --show-toplevel)
HOOKSDIR=$ROOT/tools/git_hooks
HOOKS=pre-commit
GITHOOKSDIR=$ROOT/.git/hooks

echo "Installing git hooks to local repository..."
for i in $HOOKS; do
    echo -n "Installing $HOOKSDIR/$i..."
    if test -e $GITHOOKSDIR/$i && test ! -h $GITHOOKSDIR/$i; then
	echo -n "\nWarning: $GITHOOKSDIR/$i already exists and is not a symbolic"
	echo " link. $HOOKSDIR/$i not installed."
    else
	ln -sf $HOOKSDIR/$i $GITHOOKSDIR/$i
	echo "done."
    fi
done
echo "Finished."
