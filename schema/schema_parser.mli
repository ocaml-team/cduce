(** XML Schema Documents parsing *)

open Schema_types

val schema_of_uri: string -> schema
