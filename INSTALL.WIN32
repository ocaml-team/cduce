                 Installation Notes for Windows NT/2000/XP
                 =========================================


CDuce can be executed on Microsoft Windows by using the
RedHat/Cygnus environment Cygwin freely available at

http://www.cygwin.com/

The executable needs the cygwin1.dll that is distributed 
under GPL license. This is not compatible with the CDuce license.
For this reason we do not provide binaries but give here detailed 
instructions about how to compile CDuce sources under Cygwin/Windows.


------------------------------------------------------------------------------
Prerequisites
------------------------------------------------------------------------------

Before compiling CDuce on Windows, you need to install the
following releases of the these packages (newer releases may
work):


cygwin
  http://www.cygwin.com
ocaml 3.10.0
  http://caml.inria.fr/ocaml/distrib.html
findlib 1.2.1   
  http://www.ocaml-programming.de/packages
ulex 1.0
  http://www.cduce.org/download.html
pcre-ocaml
  http://ocaml.info/home/ocaml_sources.html
ocamlnet 2.2.9
  http://www.ocaml-programming.de/packages
pxp 1.2.0
  http://www.ocaml-programming.de/packages/documentation/pxp/index_dev.html


Installation notes (you are advised to follow this order):


1) Download and execute cygwin setup.exe.
   - Choose Internet installation
   - Choose a root directory whose path has no blanks in it
   - Choose as Local Package Directory <your root>/usr/src (optional)
   - Choose the mirror closest to you
   - When asked to select the packages to install add to the default
     choices the following packages:
       Devel/
         autoconf*
         automake*
         binutils
         gcc*
         make
         patchutils
       Interpreters/
         m4
       Lib/
	 libpcre
         pcre
	 pcre-devel
     you may also find useful to install the following packages
       Editors/
         emacs or vim (no trolls)
       Net/
         ncftp
         openssh
       Devel/
         binutils
	 subversion
       Utils/
         bc
	 bzip2
         diff
	 diffutils
         file
         patch
	 patchutils
     then proceed with installation


2) Download (we suggest in /usr/src) and install Ocaml
   [For the impatient:
     ./configure
     make world.opt
     make install]


3) Download (guess where) and install findlib.

   A plain 

     ./configure
     make all
     make opt
     make install

   should work, but it is worth checking that the paths that come
   out looking right in Makefile.config after ./configure.
   Otherwise correct paths in findlib.conf and re-run configure.
   E.g.: ./configure -bindir /usr/local/bin -mandir /usr/local/man


4) Download and install ulex 
   The ocaml 3.10.0 version of ulex on cygwin requires to add the
   "-classic-display" option to ocamlbuild in the Makefile.
     make all
     make all.opt
     make install


5) Download and install pcre-ocaml. 
   If you get an error regarding dynmaically loaded library try to
   add STATIC=yes in Makefile.conf

     make
     make install
   

6) Download and install ocamlnet

   ./configure
   make all
   make opt
   make install


7) PXP:
   CDuce requires a development version >= 1.1.93.

   ./configure -without-wlex -without-wlex-compat -lexlist utf8,iso88591 -without-pp
   (there is a problem with the preprocessor in cygwin, so you
   get an error if you don't add the -without-pp)

   make all
   make opt
   make install


Now you can compile CDuce sources as indicated in the INSTALL file 
namely 

  
------------------------------------------------------------------------------
Compilation, installation
------------------------------------------------------------------------------

See the INSTALL file.




