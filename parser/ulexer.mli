open Camlp4.Sig

type token =
  | KEYWORD of string
  | IDENT of string
  | ANY_IN_NS of string
  | INT of string
  | STRING1 of string
  | STRING2 of string
  | EOI

exception Error of int * int * string

module Loc   : Loc with type t = int * int
module Token : Token with module Loc = Loc and type t = token
module Error : Error

val mk : unit -> (Loc.t -> char Stream.t -> (Token.t * Loc.t) Stream.t)

val in_comment: bool ref
val lexbuf: Ulexing.lexbuf option ref
val enc: Ulexing.enc ref
val sync: Ulexing.lexbuf -> unit

val dump_file: string -> unit
