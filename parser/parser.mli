exception Error of string

val expr : char Stream.t -> Ast.pexpr
val pat : char Stream.t -> Ast.ppat
val prog : char Stream.t -> Ast.pmodule_item list
val top_phrases : char Stream.t -> Ast.pmodule_item list

val sync : unit -> unit
val localize_exn: (unit -> 'a) -> 'a

module Gram : Camlp4.Sig.Grammar.Static

(* Hooks to extend the syntax *)

module Hook: sig
  val expr: Ast.pexpr Gram.Entry.t
  val pat: Ast.ppat Gram.Entry.t
  val keyword: string Gram.Entry.t
end

(* Helpers *)
(* TODO: put this in Ast *)

val logical_and: Ast.pexpr -> Ast.pexpr -> Ast.pexpr
val logical_or: Ast.pexpr -> Ast.pexpr -> Ast.pexpr
val logical_not: Ast.pexpr -> Ast.pexpr

val if_then_else: Ast.pexpr -> Ast.pexpr -> Ast.pexpr -> Ast.pexpr
